from scrapy.exceptions import DropItem
from sqlalchemy import create_engine


class SqlPipeline(object):
    def __init__(self, table_name: str, db_url: str, pool_size: int):
        self.table_name = table_name
        self.db = create_engine(db_url, pool_size=pool_size)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        return cls(
            table_name=crawler.settings.get("DB_TABLE_NAME"),
            db_url=crawler.settings.getint("DB_URL"),
            pool_size=crawler.settings.getint("DB_CONNECTION_POOL_SIZE"),
        )

    def process_item(self, item, spider):
        for data in item:
            if not data:
                raise DropItem("Missing {0}!".format(data))
        doc = dict(item)
        cursor = self.db.cursor()
        cursor.execute(
            f"insert into {self.table_name} values (%s, %s, %s, %s)",
            tuple(doc.values()),
        )
        self.db.commit()
        return item
